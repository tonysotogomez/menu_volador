
CREATE TABLE `menu_volador`.`drones` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `last_posx` INT NOT NULL DEFAULT 0,
  `last_posy` INT NOT NULL DEFAULT 0,
  `last_posz` VARCHAR(1) NOT NULL DEFAULT 'N',
  `active` TINYINT NOT NULL DEFAULT 1,
  `create_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `menu_volador`.`entregas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `dron_id` INT NULL,
  `posx` INT NULL,
  `posy` INT NULL,
  `posz` VARCHAR(45) NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_dron_entregas_idx` (`dron_id` ASC),
  CONSTRAINT `fk_dron_entregas`
    FOREIGN KEY (`dron_id`)
    REFERENCES `menu_volador`.`drones` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


ALTER TABLE `menu_volador`.`entregas` 
DROP FOREIGN KEY `fk_dron_entregas`;
ALTER TABLE `menu_volador`.`entregas` 
CHANGE COLUMN `dron_id` `dron_id` INT(11) NOT NULL ,
CHANGE COLUMN `posx` `posx` INT(11) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `posy` `posy` INT(11) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `posz` `posz` VARCHAR(1) NOT NULL DEFAULT 'N' ,
CHANGE COLUMN `created_at` `created_at` DATETIME NOT NULL ,
CHANGE COLUMN `deleted_at` `deleted_at` DATETIME NOT NULL ;
ALTER TABLE `menu_volador`.`entregas` 
ADD CONSTRAINT `fk_dron_entregas`
  FOREIGN KEY (`dron_id`)
  REFERENCES `menu_volador`.`drones` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
