<?php
class Conectar{
    private $driver;
    private $host, $user, $pass, $database, $charset;
  
    public function __construct() {
        $db_cfg = require_once __DIR__ .'/../config/database.php';
        $this->driver="mysql"; //$db_cfg["driver"];
        $this->host="localhost"; //$db_cfg["host"];
        $this->user="root"; //$db_cfg["user"];
        $this->pass=""; //$db_cfg["pass"];
        $this->database="menu_volador"; //$db_cfg["database"];
        $this->charset="utf8"; //$db_cfg["charset"];
    }
    
    public function conexion(){
        try {
            if($this->driver=="mysql" || $this->driver==null){
                $con=new mysqli($this->host, $this->user, $this->pass, $this->database);
                $con->query("SET NAMES '".$this->charset."'");
            }
        } catch (\Throwable $th) {
            throw $th;
        }
      
        
        return $con;
    }
    
    public function startFluent(){
        require_once __DIR__ ."/../core/FluentPDO/FluentPDO.php";
        
        if($this->driver=="mysql" || $this->driver==null){
            $pdo = new PDO($this->driver.":dbname=".$this->database, $this->user, $this->pass);
            $fpdo = new FluentPDO($pdo);
        }
        
        return $fpdo;
    }
}
?>
