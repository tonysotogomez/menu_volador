<?php
class Dron extends EntidadBase{
    private $id;
    private $name;
    private $last_posx;
    private $last_posy;
    private $last_posz;
    
    public function __construct($adapter) {
        $table="drones";
        parent::__construct($table, $adapter);
    }
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getPosX() {
        return $this->last_posx;
    }

    public function setPosX($last_posx) {
        $this->last_posx = $last_posx;
    }

    public function getPosY() {
        return $this->last_posy;
    }

    public function setPosY($last_posy) {
        $this->last_posy = $last_posy;
    }

    public function getPosZ() {
        return $this->last_posz;
    }

    public function setPosZ($last_posz) {
        $this->last_posz = $last_posz;
    }

    public function save(){
        $query="INSERT INTO drones (id,name)
                VALUES(NULL,
                       '".$this->name."');";
        $save=$this->db()->query($query);
        //$this->db()->error;
        return $save;
    }

}
?>