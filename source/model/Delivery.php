<?php
class Delivery extends EntidadBase{
    private $id;
    private $dronId;
    private $posx;
    private $posy;
    private $posz;
    
    public function __construct($adapter) {
        $table="entregas";
        parent::__construct($table,$adapter);
    }
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    
    public function getDronId() {
        return $this->dronId;
    }

    public function setDronId($dronId) {
        $this->dronId = $dronId;
    }

    public function getPosX() {
        return $this->posx;
    }

    public function setPosX($posx) {
        $this->posx = $posx;
    }

    public function getPosY() {
        return $this->posy;
    }

    public function setPosY($posy) {
        $this->posy = $posy;
    }

    public function getPosZ() {
        return $this->posz;
    }

    public function setPosZ($posz) {
        $this->posz = $posz;
    }

    public function save(){
        $query="INSERT INTO entregas (id,dronId,posx,posy,posz)
                VALUES(NULL,
                       '".$this->dronId."',
                       '".$this->posx."',
                       '".$this->posy."',
                       '".$this->posz."');";
        $save=$this->db()->query($query);
        //$this->db()->error;
        return $save;
    }

}
?>