<?php

class DronController extends ControladorBase{
    public $conectar;
	public $adapter;
	
    public function __construct() {
        parent::__construct();
        $this->conectar  = new Conectar();
        $this->adapter   = $this->conectar->conexion();
        $this->roadMax   = 3;
        $this->dronesMax = 20;
        $this->posZ      = ["N" => "Norte","S" => "Sur","O" => "Oeste","E" => "Este"];
    }
    
    public function getAllDrons() {
        $dron = new Dron($this->adapter);
        $drons = $dron->getAll();

        return $drons;
    }

    public function index(){
        $drones = $this->getAllDrons();
        $i = 1;   
        foreach ($drones as $key => $dron) {
            $number = str_pad($i, 2, "0", STR_PAD_LEFT);
            $fp = $this->getRoadFile($number);            
            $num_lineas = 0;
            $contenidoTxt = "==Reporte de entregas==\r\n\r\n";
            while (!feof($fp)){
                if ($road = fgets($fp)){
                    $num_lineas++;
                    if ($num_lineas > $this->roadMax) {
                        break;
                    }
                }
                $dronPosition = $this->readRoad($dron, $road);
                $contenidoTxt .= "(".$dronPosition->last_posx.", ".$dronPosition->last_posy. ") dirección ".$this->posZ[$dronPosition->last_posz]."\r\n\r\n";
            }
            fclose($fp);
            $i++;
            if ($i == $this->dronesMax) {
                break;
            }
            $this->generateReport($number, $contenidoTxt);
        }

    }


    public function getRoadFile($number) {
        $input = __DIR__."/../in".($number).".txt";
        if (file_exists($input)) {
            return fopen($input, "r");
        } else {
            throw new Exception("Archivo de rutas no encontrado del dron".$number);
        }
    }


    public function generateReport($number , $contenidoTxt) {
        $output = "out".($number).".txt";
        $archivo = fopen(__DIR__."/../../source/reporte/".$output,"w+");
        fwrite($archivo,$contenidoTxt);
    }

    //Lee la fila de la ruta del dron
    public function readRoad($dron, $road) {
        if (!is_object($dron)) {
           throw new InvalidArgumentException("El dron debe ser un objeto");
        }
        for($i=0;$i<strlen($road);$i++) {
            switch ($road[$i]) {
                case 'A':
                    $this->setGo($dron);
                    break;
                case 'I':
                    $this->turnLeft($dron);
                    break;
                case 'D':
                    $this->turnRight($dron);
                    break;
                default:
                    break;
            }
        }
        return $dron;
    }


    public function setGo($dron) {
        switch ($dron->last_posz) {
            case 'N':
                $dron->last_posy++;
                break;
            case 'S':
                $dron->last_posy--;
                break;
            case 'E':
                $dron->last_posx++;
                break;
            case 'O':
                $dron->last_posx--;
                break;
            default:
                break;
        }
        return $dron;
    }

    public function turnLeft($dron) {
        switch ($dron->last_posz) {
            case 'N':
                $dron->last_posz = 'O';
                break;
            case 'O':
                $dron->last_posz = 'S';
                break;
            case 'S':
                $dron->last_posz = 'E';
                break;
            case 'E':
                $dron->last_posz = 'N';
                break;
            default:
                throw new InvalidArgumentException("Dron tiene una posición inválida");
                break;
        }
        return $dron;
    }

    public function turnRight($dron) {
        switch ($dron->last_posz) {
            case 'N':
                $dron->last_posz = 'E';
                break;
            case 'E':
                $dron->last_posz = 'S';
                break;
            case 'S':
                $dron->last_posz = 'O';
                break;
            case 'O':
                $dron->last_posz = 'N';
                break;
            default:
                throw new InvalidArgumentException("Dron tiene una posición inválida");
                break;
        }
        return $dron;
    }
   

}
?>
