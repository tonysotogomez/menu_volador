<?php

use PHPUnit\Framework\TestCase;
use DronController as Dron;

class DronTest extends TestCase {

    private $dron;

    public function setUp():void {
        $this->dron = new Dron();
    }

    public function testcaseDirectoryReportExist() {
        $this->assertDirectoryExists('source/reporte');
    }

    public function testSetGo() {

        $dronTest = new stdClass(); 
        $dronTest->last_posx = 0;
        $dronTest->last_posy = 0;
        $dronTest->last_posz = 'N';

        $dron = new stdClass(); 
        $dron->last_posx = 0;
        $dron->last_posy = 1;
        $dron->last_posz = 'N';

        $this->assertEquals($dron, $this->dron->setGo($dronTest));
    }

    public function testTurnLeft() {

        $dronTest = new stdClass(); 
        $dronTest->last_posx = 0;
        $dronTest->last_posy = 0;
        $dronTest->last_posz = 'N';

        $dron = new stdClass(); 
        $dron->last_posx = 0;
        $dron->last_posy = 0;
        $dron->last_posz = 'O';

        $this->assertEquals($dron, $this->dron->turnLeft($dronTest));
    }

    public function testTurnRight() {

        $dronTest = new stdClass(); 
        $dronTest->last_posx = 0;
        $dronTest->last_posy = 0;
        $dronTest->last_posz = 'N';

        $dron = new stdClass(); 
        $dron->last_posx = 0;
        $dron->last_posy = 0;
        $dron->last_posz = 'E';

        $this->assertEquals($dron, $this->dron->turnRight($dronTest));
    }

    //El ejemplo del enunciado con la ruta AAAAIAA 
    public function testReadRoad() {

        $dronTest = new stdClass(); 
        $dronTest->last_posx = 0;
        $dronTest->last_posy = 0;
        $dronTest->last_posz = 'N';

        $dron = new stdClass(); 
        $dron->last_posx = -2;
        $dron->last_posy = 4;
        $dron->last_posz = 'O'; //En el enunciado  dice que es dirección N, sim embargo esto generaría error en el testcase

        $this->assertEquals($dron, $this->dron->readRoad($dronTest,"AAAAIAA"));
    }

    
    public function testcaseForInvalidRoad() {

        $dronTest = new stdClass(); 
        $dronTest->last_posx = 0;
        $dronTest->last_posy = 0;
        $dronTest->last_posz = 'N';

        $dron = new stdClass(); 
        $dron->last_posx = 0;
        $dron->last_posy = 0;
        $dron->last_posz = 'N';

        $this->assertEquals($dron, $this->dron->readRoad($dronTest,"123467"));
    } 

    public function testcaseForInvalidDron() {

        $this->expectException(InvalidArgumentException::class);

        $dronTest = new stdClass(); 
        $dronTest->last_posx = 0;
        $dronTest->last_posy = 0;
        $dronTest->last_posz = 'X';

        $this->dron->turnLeft($dronTest);
    }

    
    public function testcaseForNotReportFileGenerated() {

        $contendio = "TestCase";
        $this->dron->generateReport("05", $contendio);

        $filename = __DIR__."/../source/reporte/out05.txt"; 
        $this->assertFileExists( 
            $filename, 
            "Archivo de reporte no generado"
        ); 
    } 

    public function testcaseForNotRoadFileFound() {

        $num = "01";
        $this->dron->getRoadFile($num);

        $filename = __DIR__."/../source/in".$num.".txt"; 
        $this->assertFileExists( 
            $filename, 
            "Archivo de ruta no encontrado"
        ); 
    } 

}

?>